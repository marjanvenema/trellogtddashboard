// Gtd boards
// Lists
// To Do
// Waiting for response

function sortByBoardAndPos(left, right) {
  var result = left.board.guiIndex - right.board.guiIndex;
  if (result == 0)
    return left.pos - right.pos;
  else
    return result;
}

function showCards(cards, boardsSortedById, outputTarget) {
  // Filter out cards that have a due date which is more than a week in the future
  // Don't do setHours here, as that makes referenceDate an int?! At least... it then
  // no longer considers getDate() a function of referenceDate?!
  var referenceDate = new Date(); 
  referenceDate.setDate(referenceDate.getDate() + 14);
  cards = cards.filter(function (card) {
    if (card.due != null) {
      return (new Date(card.due).setHours(0, 0, 0, 0) <= referenceDate.setHours(0,0,0,0));
    } else {
      return true;
    }
  });

  if (cards.length == 0) {
    message = "No cards in list";
    $(outputTarget).append(message);
    return;
  }

  // Filter out any cards for which the board is not in the list of selected boards.
  // Add board object to card when it is found, for easy access to board properties from card.
  cards = cards.filter(function (card) {
    var boardOfCard = boardsSortedById.find(function (board) { return board.id === card.idBoard });
    if (boardOfCard != null) {
      card.board = boardOfCard;
      return true;
    } else {
      return false;
    }
  });
  cards.sort(sortByBoardAndPos);

  // Add cards to html element defined by outputTarget
  $.each(cards, function (index, card) {
    var dateFormatted = "";
    if (card.due != null) {
      dateFormatted = new Date(card.due).toLocaleDateString("nl-NL");
    }
    var cardOut = $(
      "<p><a target='blank' href='" + card.url + "'>"
      + "<span class='badge' style='background:"
      + card.board.prefs.backgroundColor + ";'>"
      + card.board.name + "</span> "
      + card.name
      + (card.due == null ? "" : (" due " + dateFormatted))
      + "</p>");
    $(outputTarget).append(cardOut);
  });
}

function loadCardsInList(listName, boardsSortedById, outputTarget) {
  // Get all cards in specified list for all boards that have this list. This may return cards on unselected boards!
  Trello.get(
    '/search',
    { query: 'list:"' + listName + '"', cards_limit: 100, card_fields: "name,pos,idBoard,url,due" })
    .success(function (cardsResponse) { showCards(cardsResponse.cards, boardsSortedById, outputTarget); })
    .error(function () { console.log("Failed to load cards in %s list.", [listName]); }
    );
}

function showBoards(boards, outputTarget) {
  $(outputTarget).append("<lu>");

  $.each(boards, function (index, board) {
    var brd = $(
      "<li id='" + board.id + "'>"
      + "<span class='badge'>"
      + board.name + "</span> "
      + "</li>"
    );
    $(outputTarget).append(brd);
    colorBoardBadge(board.id, board.hidden, board.prefs.backgroundColor);
    $("#" + board.id).click(function(){toggleBoard(board.id, board.prefs.backgroundColor); return false;});
  });

  $(outputTarget).append("</ul>");
}

function colorBoardBadge(boardId, hidden, boardColor) {
  if (hidden) {
    badgeColor = "#9C9C9C";
    badgeTextColor = "#3B3B3B";
  } else {
    badgeColor = boardColor;
    badgeTextColor = "#FFFFFF";
  }

  $("#" + boardId + "> span").css("background", badgeColor).css("color", badgeTextColor);
}

function toggleBoard(boardId, boardColor) {
  boards = JSON.parse(localStorage.boards);
  
  var toggledBoard = boards.find(function (board) { return board.id === boardId });
  if (toggledBoard != null) {
    toggledBoard.hidden = !toggledBoard.hidden;
    colorBoardBadge(boardId, toggledBoard.hidden, boardColor);
  } else {
    colorBoardBadge(boardId, true, boardColor);
  }

  localStorage.boards = JSON.stringify(boards);
}

function loadAllContent() {
  // Get all open boards for member that holds our token.
  Trello.get(
    '/members/me/boards',
    { filter: "open", fields: "id,name,url,prefs,starred" })
    .success(function (boardsSortedByName) { // Trello returns boards sorted by name.

      // Remember position in response to show boxes for each list in same order. 
      $.each(boardsSortedByName, function (index, board) {
        board.guiIndex = index;
      });

      // Sort boards by id to facilitate linking cards to the board on which they belong.
      var boardsSortedById = [];
      $.extend(boardsSortedById, boardsSortedByName);
      boardsSortedById.sort(function (left, right) {
        return ((left.id < right.id) ? -1 : ((left.id > right.id) ? 1 : 0));
      });

      // Save response to local storage
      localStorage.boards = JSON.stringify(boardsSortedById);
      //boardsSortedById = JSON.parse(localStorage.boards);

      showBoards(boardsSortedByName, "#boards");

      loadCardsInList("To Do", boardsSortedById, "#todocards");
      loadCardsInList("Waiting for Response", boardsSortedById, "#waitingcards");
    })
    .error(function () { console.log("Failed to load boards."); })
    ;
}

Trello.authorize({
  type: "popup",
  name: "Marjan's Trello GTD Dashboard",
  scope: {
    read: true,
    write: false
  },
  expiration: "never",
  success: loadAllContent,
  error: function () { console.log("Failed authentication"); }
});
