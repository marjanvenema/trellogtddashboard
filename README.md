# README #

Put these files somewhere on a web server and point your browser to the folder in which they live.

It's a Trello dashboard of sorts, so you'll need a Trello account to use this thing and it may help to have at least one open board with a "To do this week" and/or "Waiting for response" list.

### What is this repository for? ###

* Marjan learning javascript, ajax, jQuery and more.
* Version: alpha.alpha.alpha.0.0.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests: Don't we always?
* Code review: Yes please review my code. Only constructive reviews will get my attention.
* Other guidelines: Be kind. I am not exactly new when it comes to software development, but my javascript skills are very much nascent.

### Who do I talk to? ###

* Repo owner or admin: Marjan Venema
* Other community or team contact: Are you volunteering?